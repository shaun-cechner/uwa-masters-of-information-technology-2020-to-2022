-- Write and execute a query that will return all rows from the Locations table
-- that are located within forty kilometres of Crawley (-31.986S, 115.822E).
SELECT * FROM Locations
WHERE ST_Distance_Sphere(geometry, ST_GeomFromText('POINT(115.822 -31.986)')) <= 40 * 1000;