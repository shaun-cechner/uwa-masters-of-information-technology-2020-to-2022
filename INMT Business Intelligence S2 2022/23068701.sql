/*
4th September 2022
Shaun Cechner 23068701
Individual Assessment 1, due 30th September
*/

-- 1a. Create the warehouse table
USE 23068701_db;

CREATE TABLE Warehouse(
    id INT NOT NULL AUTO_INCREMENT, -- unique identifier
    name VARCHAR(99),
    productCapacity INT, -- this capacity is a fixed number of products, but can be made up of any products
    location POINT,
    PRIMARY KEY (id)
);

-- 1b. Create the inventory item table
CREATE TABLE InventoryItem(
    serialNumber INT NOT NULL, -- TODO a unique serial number (in the same format for every product),
    warehouseId INT,
    createdDate DATE NOT NULL,
    productName VARCHAR(99),
    costPrice FLOAT,
    salePrice FLOAT,
    hasGst BOOLEAN,
    details VARCHAR(255),  -- product details (e.g. a short description that may uniquely identify different modifications for each product – including information such as screen size, storage size, amount of memory and so forth)
    PRIMARY KEY (serialNumber),
    FOREIGN KEY (warehouseId) REFERENCES Warehouse(id) -- Both tables should be related to each other in the simplest manner possible
);

-- 2. You should generate and insert reasonable sample data (rows/observations) for the tables you create
-- 2.a 3 warehouses with positive stock level
INSERT INTO Warehouse (id, name, productCapacity, location)
VALUES
    (1, "Perth", 200, ST_GeomFromText('POINT(115.850 -31.950)')),
    (2, "Joondalup", 100, ST_GeomFromText('POINT(115.766 -31.745)')),
    (3, "Mandurah", 50, ST_GeomFromText('POINT(115.723 -32.523)'));

-- 2.b 20 inventory items -- (taken from PLE.com.au)
INSERT INTO InventoryItem (serialNumber, createdDate, productName, costPrice, salePrice, hasGst, details, warehouseId)
VALUES
    (10000, "2022-07-01", 
        "InWin BL641 Black mATX Desktop Chassis w/300W 80PLUS Gold PSU", 
        99, 159, 1, 
        "A small 11.5L SFF chassis with optimal thermal and acoustic performance without additional system fan",
        1), -- warehouseId
    (10001, "2022-02-01", 
        "ASUS Laptop D515DA 15.6 Ryzen 3 Windows 11 Notebook", 
        300, 599, 1, 
        "Whether for work or play, ASUS d515 is the entry level laptop that delivers powerful performance and immersive visuals. Its NanoEdge display boasts wide 178 viewing qngels and a matte anti-glare coating for a truly engaging experience.", 
        1), -- warehouseId
    (10002, "2021-10-01", 
        "Logitech G Pro X Superlight Cordless Gaming Mouse - Black", 
        100, 180, 0, 
        "Features extra weight reduction design with frictionless glide, pro-grade wireless, advanced precision HERO sensor, and more", 
        1), -- warehouseId
    (10003, "2022-01-01", 
        "Corsair Scimitar RGB Elite Black Gaming Mouse",
        50, 109, 0, "The SCIMITAR RGB ELITE levels up your gameplay with 17 fully programmable buttons, a patented Key Slider™ control system, and an 18,000 DPI optical sensor", 
        1), -- warehouseId
    (10004, "2022-06-07", 
        "BattleBull Combat Gaming Chair Black/Blue", 
        70, 199, 1, "Introducing the BattleBull Combat Gaming Chair! Utilising soft PU leather, memory foam cushioning and steel belt elastics, the BattleBull Combat Gaming chair offer incredible comfort and support. Sit in your throne while smashing your adversaries!", 
        1), -- warehouseId
    (10005, "2022-03-15", 
        "EVGA Z20 RGB Optical Mechanical Gaming Keyboard (Linear)", 
        50, 80, 1, "Logitech Wireless Keyboard", 
        1), -- warehouseId
    (10006, "2022-01-01", 
        "SteelSeries QcK Cloth Gaming Mousepad - Medium", 
        5, 15, 0, 
        "QcK mousepads are the top choice of esports pros. The legendary micro-woven cloth has made the QcK line the world’s best-selling gaming surface", 
        1), -- warehouseId
    (10007, "2022-07-01", 
        "BenQ DesignVue PD3200U 32 UHD 4K 60Hz 4MS IPS LED Professional Monitor", 
        600, 999, 1, 
        "Design professionals never compromise on details. BenQ Designer Monitors deliver absolute color precision and ultra detailed high resolution to craft every step leading to brilliant work, turning design dreams into reality", 
        2), -- warehouseId
    (10008, "2022-07-15", 
        "SteelSeries Arctis Pro + GameDAC - Black", 
        200, 389, 0, 
        "Arctis Pro + GameDAC delivers gaming’s first certified Hi-Res Audio system, ensuring you hear high-fidelity, full-resolution audio with no down-sampling", 
        2), -- warehouseId
    (10009, "2022-08-12", 
        "ALOGIC Clarity 27 UHD 4K 60Hz 4ms HDR600 IPS Monitor", 
        700, 869, 1, 
        "ALOGIC Clarity 27 UHD 4K Monitor is a revolutionary lifestyle-inspired business grade monitor that brings your digital media to life. With thin-surround bezel across seamless glass, users can now enjoy larger and immersive viewing.", 
        2), -- warehouseId
    (10010, "2022-01-01", 
        "Razer Hanbo Chroma RGB AIO Liquid Cooler 360MM (aRGB Pump Cap)", 
        150, 299, 0, 
        "Keep your CPU temperatures cooler at any load with the Razer Hanbo—an All-In-One Liquid Cooler (AIO) designed for maximum thermal performance.", 
        2), -- warehouseId

    (10011, "2021-05-07", 
        "TP-LINK Deco X20 Wireless-AX1800 WiFi 6 Mesh Router - 2 Pack", 
        200, 279, 0, 
        "Armed with Wi-Fi 6 technology, Deco whole home mesh Wi-Fi is designed to deliver a huge boost in coverage, speed, and total capacity", 
        2), -- warehouseId
    (10012, "2021-08-09", 
        "Logitech G29 Driving Force Racing Wheel for PC and PlayStation 5/4", 
        300, 369, 1, 
        "All your controls are where you can reach them. The D-Pad, buttons and paddle shifters are incorporated into the racing wheel", 
        2), -- warehouseId
    (10013, "2021-06-04", 
        "Playseat Evolution Driving Simulator - White", 
        400, 549, 0, 
        "Feel the evolution! Take a seat and drive as fast as possible in the Playseat® Evolution White! Playseat chairs are used by professional drivers", 
        3), -- warehouseId
    (10014, "2022-07-01", 
        "Lian-Li PC-O11 Dynamic XL ROG Certified Full Tower Case - White", 
        200, 239, 1, 
        "Built with the user's feedback in mind, the O11D XL is set to become the PC chassis of choice for custom loop water cooling, workstation powerhouse build, and gaming enthusiasts for maximum customization", 
        3), -- warehouseId
    (10015, "2022-03-01", 
        "Edifier R1380DB 2.0 Professional Bluetooth Bookshelf Speakers", 
        100, 159, 0, 
        "The clean wooden finish of the Edifier R1380DB bookshelf speakers leaves a lasting impression of elegance and luxury", 
        3), -- warehouseId
    (10016, "2022-11-01", 
        "Seagate BarraCuda ST2000LM015 2.5 2TB 128MB 5400RPM Laptop HDD", 
        99, 125, 0, 
        "Get the most out of your storage with BarraCuda hard drives. From computers full of photos and memories to gaming PCs that need more room to play, BarraCuda grows with you", 
        3), -- warehouseId
    (10017, "2022-10-01", 
        "WD Blue WD10SPZX 2.5 1TB 128MB 5400RPM SMR Mobile HDD", 
        50, 65, 1, 
        "Boost your PC storage with WD Blue drives, the brand designed just for desktop and all-in-one PCs with a variety of storage capacities",
        3), -- warehouseId
    (10018, "2022-03-01", 
        "WD Blue WD20SPZX 2.5 2TB 128MB 5400RPM SMR Mobile HDD", 
        99, 129, 1, 
        "WD Blue hard drives are designed for use as primary drives in notebooks and external enclosures",
        1), -- warehouseId
    (10019, "2022-04-01", 
        "WD Blue WD5000LPZX 2.5 500GB 16MB 5400RPM Mobile HDD", 
        30, 59, 1, 
        "Built to WD’s high standards of quality and reliability, WD Blue mobile hard drives offer the features that are ideal for your everyday mobile computing needs", 
        1); -- warehouseId


-- 3. Change the GST status of the second inventory item to TRUE, regardless of its current value.
UPDATE InventoryItem
SET hasGst = 1
WHERE serialNumber = 10001;

-- 4. Change the first warehouse’s product capacity to half of its initial value.
UPDATE Warehouse
SET productCapacity = (productCapacity / 2)
WHERE id = 1;

--5.  Delete the third warehouse (only) – do not ‘run’ it
DELETE FROM Warehouse
WHERE id = 3;

-- 6. Delete the first inventory item (only) – do not ‘run’ it.
DELETE FROM InventoryItem
WHERE serialNumber = 10000;

-- 7. Modify the inventory table to calculate the amount of GST included within the sale price, generated automatically from other columns.
ALTER TABLE InventoryItem ADD COLUMN taxAmount FLOAT GENERATED ALWAYS AS (hasGst * (salePrice / 11)) STORED;

-- 8. View the CREATE TABLE statement for each table.
SHOW CREATE TABLE Warehouse;
SHOW CREATE TABLE InventoryItem;

-- 9. View all inventory in the first warehouse, from the most expensive first (by sale price) to the cheapest.
SELECT * FROM InventoryItem
WHERE warehouseId = 1 
ORDER BY salePrice DESC;

-- 10. View the cheapest and most expensive product in each warehouse.
-- 10.a Cheapest products for each warehosue
SELECT salePrice, productName AS cheapestProductName, warehouseId 
FROM InventoryItem 
WHERE salePrice = (SELECT MIN(salePrice) FROM InventoryItem WHERE warehouseId = 1);

SELECT salePrice, productName AS cheapestProductName, warehouseId 
FROM InventoryItem 
WHERE salePrice = (SELECT MIN(salePrice) FROM InventoryItem WHERE warehouseId = 2);

SELECT salePrice, productName AS cheapestProductName, warehouseId 
FROM InventoryItem 
WHERE salePrice = (SELECT MIN(salePrice) FROM InventoryItem WHERE warehouseId = 3);

-- 10.a Most expensive products for each warehosue
SELECT salePrice, productName AS mostExpensiveProductName, warehouseId 
FROM InventoryItem 
WHERE salePrice = (SELECT MAX(salePrice) FROM InventoryItem WHERE warehouseId = 1);

SELECT salePrice, productName AS mostExpensiveProductName, warehouseId 
FROM InventoryItem 
WHERE salePrice = (SELECT MAX(salePrice) FROM InventoryItem WHERE warehouseId = 2);

SELECT salePrice, productName AS mostExpensiveProductName, warehouseId 
FROM InventoryItem 
WHERE salePrice = (SELECT MAX(salePrice) FROM InventoryItem WHERE warehouseId = 3);

-- 11. View the name and capacity of all warehouses, from the one with the largest capacity first to the smallest capacity.
SELECT name, productCapacity
FROM Warehouse 
ORDER BY productCapacity DESC;

-- 12. Summarise the amount of stock in each warehouse.
SELECT COUNT(*), warehouseId
FROM InventoryItem
GROUP BY warehouseId;

-- 13. Summarise how many products were created in each month.
SELECT COUNT(serialNumber), MONTH(createdDate), YEAR(createdDate)
FROM InventoryItem
GROUP BY YEAR(createdDate), MONTH(createdDate);

-- 14. Determine how many warehouses are located within 10 kilometers of the UWA Crawley Campus.
SELECT COUNT(*) FROM Warehouse WHERE ST_Distance_Sphere(location, ST_GeomFromText('POINT(115.822 -31.986)')) <= 10 * 1000;

-- 15. Create a view which joins the inventory items to the warehouses they belong in.
CREATE VIEW WarehouseItems AS
    SELECT * FROM Warehouse
    INNER JOIN InventoryItem ON InventoryItem.warehouseId = Warehouse.id;
