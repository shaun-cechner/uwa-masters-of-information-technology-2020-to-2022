INSERT INTO 23068701_db.TransactionItem (description, quantity, unitPrice, totalPrice, hasGST)
VALUES
  ("Basic Widget", 1, 123.45, 123.45, 1),
  ("Product B", 1, 99.95, 99.95, 1),
  ("Other Service", 2, 100.00, 200.00, 0),
  ("Business Intelligence", 1, 42.42, 42.42, 0);
