-- Modify your TransactionItem table such that you now have a new attribute named hasGst, which can either be True or False.
ALTER TABLE 23068701_db.TransactionItem ADD COLUMN hasGST BOOLEAN;
