USE 23068701_db;
-- sort by the following
-- descriptions in the TransactionItem from Z to A
SELECT description FROM TransactionItem ORDER BY description DESC;

-- whether or not the item has GST applied (from No to Yes) and 
-- its unit price (smallest to largest) in the TransactionITem table
SELECT hasGst, unitPrice FROM TransactionItem ORDER BY hasGst ASC, unitPrice ASC;

-- items in the transaction table from newest to oldest
SELECT * FROM Transaction ORDER BY date DESC;

-- items in the transaction table from newest to oldest and 
-- then by customer name from Z to A
SELECT * FROM Transaction ORDER BY date DESC, customerName DESC;