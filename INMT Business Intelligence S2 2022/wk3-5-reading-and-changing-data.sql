-- Adjust the price of “Product B” in your table to be 199.95. (You will need to change two columns!)
UPDATE 23068701_db.TransactionItem
SET unitPrice = 199.95, totalPrice = 199.95
WHERE description = "Product B";
