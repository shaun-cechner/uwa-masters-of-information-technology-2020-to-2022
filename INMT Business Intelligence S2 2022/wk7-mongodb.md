Created a mongodb db https://cloud.mongodb.com

```sh
mongosh "mongodb+srv://cluster0.s32txyd.mongodb.net/myFirstDatabase" --apiVersion 1 --username cechner
```

```sql
-- create a collection (“table”) to store our many documents
db.createCollection("transactions")

-- add one or more documents to the collection
db.transactions.insertMany([
    { id: 1, description: "Basic Widget", quantity: 1, unitPrice: 123.45, totalPrice: 123.45, hasGst: true},
    { id: 2, description: "Product B", quantity: 1, unitPrice: 99.95, totalPrice: 99.95, hasGst: true},
    { id: 3, description: "Other Service", quantity: 2, unitPrice: 100.00, totalPrice: 100.00, hasGst: false},
    { id: 4, description: "Business Intelligence", quantity: 1, unitPrice: 42.42, totalPrice: 42.42, hasGst: false},
    ])
```

Find details

```sql
-- Details on all items with unit prices less than $100;
db.transactions.find({unitPrice: {$lt: 100.0}}).pretty()

-- Details on all items which do not have GST;
db.transactions.find({hasGst: false}).pretty()

-- Details on all items with a quantity greater than 1;
db.transactions.find({quantity: {$gt: 1}}).pretty()

-- Details on all items except the one with an ID of 3.
db.transactions.find({id: {$ne: 3}}).pretty()

```
