
-- The description and unit price of items greather than $110.00 in unit price
SELECT description, unitPrice FROM 23068701_db.TransactionItem WHERE (unitPrice > 110.00);

-- All details of items which have a quantity that is not equal to one
SELECT * FROM 23068701_db.TransactionItem WHERE (quantity != 1);

-- Descriptions of items that have a description beginning with the letter B;
SELECT description FROM 23068701_db.TransactionItem WHERE (description LIKE "B%");

-- The description and unit price of items which are less than $201.00 in unit price and have GST applied to them
SELECT description FROM 23068701_db.TransactionItem WHERE ((unitPrice < 201.00) AND (hasGst = 1));
