USE 23068701_db;
-- Create two views on the database table, named and containing the content as stated below:

-- A view named TransactionsWithItems which contains the Transaction and TransactionItem tables joined on common (matching) rows;
CREATE VIEW TransactionsWithItems AS
SELECT * FROM Transaction
INNER JOIN TransactionItem ON TransactionItem.id = Transaction.transactionItem;

-- A view named GstOnlyItems which contains only TransactionItem rows/observations that contain GST;
CREATE VIEW GstOnlyItems AS
SELECT * FROM TransactionItem
WHERE hasGst = 1;

-- A view named JanuaryTransactions which contains only Transaction rows/observations that are dated within January.
CREATE VIEW JanuaryTransactions AS
SELECT * FROM Transaction
WHERE MONTH(date) = 01;