USE 23068701_db;
-- Create and execute queries to join the TransactionItem table on to the
-- Transaction table (with the latter as the ‘first’) on the id / transactionItem:
-- Rows that exist in both tables;
SELECT * FROM Transaction
INNER JOIN TransactionItem ON TransactionItem.id = Transaction.transactionItem;

-- Rows that exist only within the TransactionItem table;
SELECT * FROM Transaction
RIGHT JOIN TransactionItem ON TransactionItem.id = Transaction.TransactionItem;

-- Rows that exist only within the Transaction table.
SELECT * FROM Transaction
LEFT JOIN TransactionItem ON TransactionItem.id = Transaction.TransactionItem;
