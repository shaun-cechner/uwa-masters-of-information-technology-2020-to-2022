# INMT Business Intelligence

This unit reflects the following UN Sustainable Development Goals:
- Number 4: Quality Education;
- Number 8: Decent Work and Economic Growth;
- Number 9: Industry, Innovation and Infrastructure;
- Number 12: Responsible Consumption and Production

## Description
This unit enables students to understand and apply flexible computer-based systems that learn to solve problems—genetic algorithm, fuzzy logic and neural networks.\
This is in a business management and planning environment.\
Hybrid methods and the relatively new swarm intelligence models are also introduced.

## Outcomes
Students are able to:
1) **describe** a range of advanced technologies appropriate for business intelligence;
2) develop a hands-on capacity to apply BI methods;
3) assess the potential to apply BI techniques in business and administrative contexts;
4) **compare** various BI technology approaches to solving business problems; and
5) **interpret** the output of BI techniques in a business context.

## Assessments
Indicative assessments in this unit are as follows:
1) **applications**
2) reading **report**; and
3) **project**. Further information is available in the unit outline.

Student may be offered supplementary assessment in this unit if they meet the eligibility criteria.

## Connecting

- <a href="https://unidesk.uwa.edu.au/" target="_blank">Unidesk</a>
- Open `MySQL Shell` app
- `\connect <StudentId>@db.tris.id.au`
- `\sql
- `\quit`

**Connecting using Powershell**
- Open `Windows PowerShell`
- I created a `mysql.bat` file in `H:\My Documents` that opens mysqlsh
- `'C:\Program Files\MySQL\MySQL Shell 8.0\bin\mysqlsh.exe'`

## Change your password
- `\connect <StudentId>@db.tris.id.au`
- `\sql`
- `SET PASSWORD = ‘<new password>’;`
- `\quit`

## Run a file from `sql` mode
`\. filename.sql`

## Other useful SQL Commands
```sql
-- Show databases
SHOW databases;

-- Show tables in a database
SHOW tables IN databaseName;

-- Use a database so you don't need to reference it each time
USE databaseName;

-- Import a csv file
LOAD DATA INFILE 'c:/tmp/discounts.csv' 
INTO TABLE discounts 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
```

## Projects

### Individual Project

### Group Project

## Weeks

### Week 1 - Intro to BI Concepts

### Week 2 - Intro to Databases

### Week 3 - CRUD Operations
**Scripts**

- [Create a database](https://gitlab.com/shaun-cechner/uwa-masters-of-information-technology-2020-to-2022/-/blob/e2cca998ec3996a1508078ad194383a2f3255cfa/INMT%20Business%20Intelligence%20S2%202022/wk3-1-create-a-database.sql)
- [Create a table](https://gitlab.com/shaun-cechner/uwa-masters-of-information-technology-2020-to-2022/-/blob/main/INMT%20Business%20Intelligence%20S2%202022/wk3-2-create-a-table.sql)
- [Modify a table](https://gitlab.com/shaun-cechner/uwa-masters-of-information-technology-2020-to-2022/-/blob/main/INMT%20Business%20Intelligence%20S2%202022/wk3-3-modify-a-table.sql)
- [Adding data](https://gitlab.com/shaun-cechner/uwa-masters-of-information-technology-2020-to-2022/-/blob/main/INMT%20Business%20Intelligence%20S2%202022/wk3-4-adding-data.sql)
- [Reading and changing data](https://gitlab.com/shaun-cechner/uwa-masters-of-information-technology-2020-to-2022/-/blob/main/INMT%20Business%20Intelligence%20S2%202022/wk3-5-reading-and-changing-data.sql)

### Week 4 - More CRUD Operations and Analysis Shortcuts
**Scripts**

- [Generate total price](https://gitlab.com/shaun-cechner/uwa-masters-of-information-technology-2020-to-2022/-/blob/b2eb1d202518c8e5a07204d4ac49d271f0dab78e/INMT%20Business%20Intelligence%20S2%202022/wk4-1-generate-total-price.sql)
- [Generate GST](https://gitlab.com/shaun-cechner/uwa-masters-of-information-technology-2020-to-2022/-/blob/b2eb1d202518c8e5a07204d4ac49d271f0dab78e/INMT%20Business%20Intelligence%20S2%202022/wk4-1-generate-total-price.sql)
- [Add constraints](https://gitlab.com/shaun-cechner/uwa-masters-of-information-technology-2020-to-2022/-/blob/b2eb1d202518c8e5a07204d4ac49d271f0dab78e/INMT%20Business%20Intelligence%20S2%202022/wk4-3-add-constraints.sql)

### Week 5 - Sorting, Grouping and More Tips

### Week 6 - Advanced Database Topics
