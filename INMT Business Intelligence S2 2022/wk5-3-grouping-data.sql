USE 23068701_db;
-- Write (and execute) queries to aggregate and select the following data as explained by each dot point below:

SELECT 'The average unit price of each TransactionItem, grouped by GST status' AS '';
SELECT hasGst, AVG(unitPrice)
FROM TransactionItem
GROUP BY hasGst;

SELECT 'The number of transactions in each month and year in the Transaction table' AS '';
SELECT COUNT(id), MONTH(date), YEAR(date)
FROM Transaction
GROUP BY YEAR(date), MONTH(date);

SELECT 'The smallest total price of each TransactionItem, grouped by the quantity first and GST status second' AS '';
SELECT MIN(totalPrice), quantity, hasGst
FROM TransactionItem
GROUP BY quantity, hasGst;

SELECT 'Average unit price of each TransactionItem costing over $100 per unit, grouped by GST' AS '';
SELECT avg(UnitPrice), hasGst
FROM TransactionItem
WHERE (unitPrice > 100)
GROUP BY hasGst;