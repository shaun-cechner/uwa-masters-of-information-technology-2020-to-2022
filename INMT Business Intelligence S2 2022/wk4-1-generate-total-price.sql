-- drop the totalPrice column
ALTER TABLE 23068701_db.TransactionItem DROP COLUMN totalPrice;

-- recreate the totalPrice column, it should be generated from other columns
ALTER TABLE 23068701_db.TransactionItem ADD COLUMN totalPrice FLOAT GENERATED ALWAYS AS (unitPrice * quantity) STORED;
