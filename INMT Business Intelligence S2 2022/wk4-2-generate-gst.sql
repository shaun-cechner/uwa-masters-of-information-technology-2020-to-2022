-- Create a column called taxAmount which determines the value of the GST
-- on each item from totalPrice
ALTER TABLE 23068701_db.TransactionItem ADD COLUMN taxAmount FLOAT GENERATED ALWAYS AS (hasGst * (unitPrice / 11)) STORED;
