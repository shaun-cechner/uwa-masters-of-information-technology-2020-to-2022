-- Create a new table named Transaction that contains a transaction identifier, a
-- transaction date, the customer name and a reference to a particular row
-- within the TransactionItem table.

CREATE TABLE 23068701_db.Transaction (
    id INT AUTO_INCREMENT,
    date DATE NOT NULL, 
    customerName VARCHAR(99) NOT NULL,
    transactionItem INT, 
    PRIMARY KEY (id), 
    FOREIGN KEY (transactionItem) REFERENCES 23068701_db.TransactionItem(id));

-- Fill the new Transaction table with data
INSERT INTO 23068701_db.Transaction (date, customerName, transactionItem) 
VALUES 
    ("2022-01-01", "Example Corporation", 4),
    ("2022-04-01", "Nikola Limited", 2),
    ("2022-04-01", "Pear Computers", 1),
    ("2022-07-31", "Western Mining", 3);
