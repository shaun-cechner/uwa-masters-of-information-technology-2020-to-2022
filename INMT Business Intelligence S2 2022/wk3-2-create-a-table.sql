CREATE TABLE 23068701_db.TransactionItem(
  id int NOT NULL AUTO_INCREMENT,
  description varchar(99),
  quantity int,
  unitPrice float,
  totalPrice float,
  PRIMARY KEY (id)
);
