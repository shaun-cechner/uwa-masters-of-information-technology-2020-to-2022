USE 23068701_db;
-- Create a new Table named Locations with data
CREATE TABLE Locations (
    id INT AUTO_INCREMENT,
    name VARCHAR(99),
    geometry POINT,
    PRIMARY KEY (id)
);

INSERT INTO Locations (name, geometry)
VALUES
    ("Perth", ST_GeomFromText('POINT(115.850 -31.950)')),
    ("Joondalup", ST_GeomFromText('POINT(115.766 -31.745)')),
    ("Mandurah", ST_GeomFromText('POINT(115.723 -32.523)')),
    ("Armadale", ST_GeomFromText('POINT(116.015 -32.153)'));
