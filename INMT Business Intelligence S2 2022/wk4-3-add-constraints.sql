-- Modify TransactionItem table to add in four additional constraints
-- 1. unitPrice and quantity must be greater than 0 (two constraints)
ALTER TABLE 23068701_db.TransactionItem ADD CHECK (unitPrice > 0);
ALTER TABLE 23068701_db.TransactionItem ADD CHECK (quantity > 0);

-- 2. hasGst should be set to FALSE(0) as a default value;
ALTER TABLE 23068701_db.TransactionItem MODIFY hasGst BOOL DEFAULT 0;

-- 3. description should not be null (look back to last week!);
ALTER TABLE 23068701_db.TransactionItem MODIFY description VARCHAR(255) NOT NULL;
