Due 30th Sept

# The Purpose

The purpose of this assessment is to demonstrate and utilise the skills learnt within the ‘Databases’ section of the Business Intelligence course, to help solve a business problem.

# The Brief

Pear Computers is building a new system to **manage the inventory of their various warehouses** throughout the Perth metropolitan area. Each warehouse has: 
- a **location** (latitude and longitude), 
- a **name**, 
- a **unique identifier** and 
- a **product capacity** of the warehouse (this capacity is a fixed number of products, but can be made up of any
products).

Each inventory item will have: 
- a **unique serial number** (in the same format for every product), 
- a **creation date** (but not time), 
- a **product name**, 
- the product’s **cost price**, 
- the product’s **sale price**, 
- whether or not the product has **GST applied** and 
- product **details** (e.g. a short description that may uniquely identify different modifications for each product – including information such as screen size, storage size, amount of memory and so forth).

- [X] Both tables should be **related to each other** in the simplest manner possible. 
- [X] A **View** should be created on a join of these two tables. 
- [X] You should generate and insert reasonable **sample data** (rows/observations) for the tables you create – **three warehouses and twenty inventory items** – ensuring each warehouse has a positive stock level.

You may wish to **look at the report questions first** to ensure that your sample data will enable answering these questions.

# The Task

Your task is two-fold for this unit: 
- firstly, to **create the database model and insert the required data** as described above (and below) and 
- secondly to analyse this data and **prepare a short (one-page) report**.

With respect to the database content, you should write the queries (with comments indicating what query does what) to achieve the following:
1. Creation of **a table for each of the specified entities**, including the correct
definition of attributes, their data types and their modifiers (including foreign and
primary keys) where these are reasonable based on the attributes’ purpose;
2. Creation of **queries** to create each of the sample data observations/rows within each of the tables for the entities as defined above;
3. A **query** to **change the GST status of the second inventory item to TRUE**,
regardless of its current value.
4. A **query** to **change the first warehouse’s product capacity to half of its initial value**.
5. A **query**  to **delete the third warehouse** (only) – do not ‘run’ it.
6. A **query**  to **delete the first inventory item** (only) – do not ‘run’ it.
7. A **query**  to modify the inventory table to **calculate the amount of GST included within the sale price**, generated automatically from other columns.
8. Queries to view the CREATE TABLE statement for each table.
9. A **query**  to **view all inventory in the first warehouse**, from the most expensive first (by sale price) to the cheapest.
10. A **query**  to **view the cheapest and most expensive product** in each warehouse.
11. A **query**  to **view the name and capacity of all warehouses**, from the one with the
largest capacity first to the smallest capacity.
12. A **query**  to **summarise the amount of stock** in each warehouse.
13. A **query**  to **summarise how many products** were created in each month.
14. A **query**  to **determine how many warehouses are located within 10 kilometers** of
the UWA Crawley Campus.
15. A **query**  to create **a view which joins the inventory items to the warehouses** they
belong in.

With respect to the short report, you should answer the following questions in one page or less in a Word-processed document:
1. What is the price of the cheapest and most expensive product in each
warehouse? Which warehouse has the cheapest overall product?
2. What is the capacity of each warehouse? Which one is smallest?
3. Which warehouse has the most stock at this point in time?
4. Which month and year contained the most products being created?
5. Which warehouses are located within 10 kilometres of the UWA Crawley
Campus?
6. Which type of analytics have we undertaken (descriptive, prescriptive, diagnostic
or predictive)? Briefly (in a paragraph or less) justify your answer.
7. Briefly (in a paragraph or less) describe one benefit in undertaking this process
using a database, as opposed to a spreadsheet.
8. Briefly (in a paragraph or less) reflect upon your completion of this task,
identifying any issues that you had and the most enjoyable part of the process.

# Submission Requirements

Please submit two files for this assessment using the submission point under the
“Individual Assessment” link on the LMS site for this unit:
- `<StudentID>.sql` file, with the commands to be run to achieve the above,
named with your Student ID;
- `<StudentID>.docx` file, with the short report as specified above, named with
your Student ID

The assessment is **due at 11:59AM (Perth time – around lunchtime) on the 30th of
September, 2022**. All late submission applications for consideration should be made
using the online UWA systems, as detailed in the first workshop. If they are not made
using this system, they will not be considered.

# Marking Guidelines

Please consult LMS for a rubric that will be available on the submission point, closer
to the due date. Most marks will be awarded for the queries.
